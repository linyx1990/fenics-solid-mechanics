
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <pybind11/operators.h>

#include <dolfin/la/GenericVector.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/fem/SystemAssembler.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/fem/Form.h>
#include <dolfin/function/Function.h>
#include <dolfin/nls/NewtonSolver.h>

#include <fenics-solid-mechanics/QuadratureFunction.h>
#include <fenics-solid-mechanics/PlasticityProblem.h>

namespace py = pybind11;
namespace fsm = fenicssolid;

namespace fenicssolid_wrappers
{
  void plasticity_problem(py::module& m)
  {
    py::class_<fsm::PlasticityProblem, std::shared_ptr<fsm::PlasticityProblem>, dolfin::NonlinearProblem>
      (m, "PlasticityProblem", "Class PlasticityProblem")
      .def(py::init<std::shared_ptr<const dolfin::Form>,
           std::shared_ptr<const dolfin::Form>,
           std::shared_ptr<dolfin::Function>,
           std::shared_ptr<fsm::QuadratureFunction>,
           std::shared_ptr<fsm::QuadratureFunction>,
           const std::vector<std::shared_ptr<const dolfin::DirichletBC>>,
           std::shared_ptr<const fsm::PlasticityModel>>(), "Create a PlasticityProblem")
      .def(py::init<std::shared_ptr<const dolfin::Form>,
           std::shared_ptr<const dolfin::Form>,
           std::shared_ptr<dolfin::Function>,
           std::shared_ptr<fsm::QuadratureFunction>,
           std::shared_ptr<fsm::QuadratureFunction>,
           const std::vector<std::shared_ptr<const dolfin::DirichletBC>>,
           std::shared_ptr<const fsm::PlasticityModel>,
           std::shared_ptr<dolfin::NewtonSolver>>(), "Create a PlasticityProblem")
      .def("form", &fsm::PlasticityProblem::form)
      .def("F", &fsm::PlasticityProblem::F)
      .def("J", &fsm::PlasticityProblem::J)
      .def("update_history", &fsm::PlasticityProblem::update_history)
      .def_readonly("eps_p_data", &fsm::PlasticityProblem::eps_p_data)
      .def_readonly("eps_p_eq_data", &fsm::PlasticityProblem::eps_p_eq_data)
      .def_readwrite("parameters", &fsm::PlasticityProblem::parameters);
  }
}
