import ufl
import dolfin

from .fsm_cpp import status_update
from .fsm_cpp import plasticity_model

#       .def(py::init<std::shared_ptr<const dolfin::Mesh>, 
#            std::shared_ptr<const dolfin::FiniteElement>,
#            const std::vector<double>&>(), "Create a QuadratureFunction")
#       .def(py::init<std::shared_ptr<const dolfin::Mesh>,
#            std::shared_ptr<const dolfin::FiniteElement>,
#            std::shared_ptr<fsm::StateUpdate>,
#            const std::vector<double>&>(), "Create a QuadratureFunction")

#fsm_constitutive_update = ConstitutiveUpdate(u, Vs, J2)

class ConstitutiveUpdate():

    def __init__(self, *args, **kwargs):
        """Initialize Function."""

        if len(args) != 3:
            raise RuntimeError("Expecting 3 arguments, got ", len(args), ".")

        if not isinstance(args[0], dolfin.Function):
            raise RuntimeError("Expecting a 'dolfin.Function' as argument 1, got ", type(args[0]), ".")
        self.Function = args[0]
        if not isinstance(args[1], dolfin.function.functionspace.FunctionSpace):
            raise RuntimeError("Expecting a 'dolfin FunctionSpace' as argument 2, got ", type(args[1]), ".")
        self.Vs = args[1]
        if not isinstance(args[2], plasticity_model.PlasticityModel):
            raise RuntimeError("Expecting a 'PlasticityModel' as argument 3, got ", type(args[2]), ".")
        self.PlasticityModel = args[2]

        self.Element = self.Vs.element()
        self.DofMap = self.Vs.dofmap()
        self._cpp_object = status_update.ConstitutiveUpdate(self.Function.cpp_object(), self.Element, self.DofMap, self.PlasticityModel)

    def name(self):
        return self._cpp_object.name()
    def w_stress(self):
        return self._cpp_object.w_stress()
    def w_tangent(self):
        return self._cpp_object.w_tangent()
    def cpp_object(self):
        return self._cpp_object
    def update(self):
        return self._cpp_object.update()
    def update_history(self):
        return self._cpp_object.update_history()
    def eps_p_eq(self):
        return self._cpp_object.eps_p_eq()
