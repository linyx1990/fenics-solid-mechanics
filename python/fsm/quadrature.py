# -*- coding: utf-8 -*-
"""Main module for FEniCS Solid Mechanics"""

# flake8: noqa

# Copyright (C) 2017 Chris N. Richardson and Garth N. Wells
#
# Distributed under the terms of the GNU Lesser Public License (LGPL),
# either version 3 of the License, or (at your option) any later
# version.

import ufl
import dolfin

from .fsm_cpp import quadrature_function
from fsm import update

#       .def(py::init<std::shared_ptr<const dolfin::Mesh>, 
#            std::shared_ptr<const dolfin::FiniteElement>,
#            const std::vector<double>&>(), "Create a QuadratureFunction")
#       .def(py::init<std::shared_ptr<const dolfin::Mesh>,
#            std::shared_ptr<const dolfin::FiniteElement>,
#            std::shared_ptr<fsm::StateUpdate>,
#            const std::vector<double>&>(), "Create a QuadratureFunction")

class QuadratureFunction(ufl.Coefficient):

    def __init__(self, *args, **kwargs):
        """Initialize Function."""
        
        if not isinstance(args[0], dolfin.function.functionspace.FunctionSpace):
            raise RuntimeError("Expecting a 'dolfin FunctionSpace' as argument 1, got ", type(args[0]), ".")
        self.Mesh = args[0].mesh()
        self.Element = args[0].element()
        if not isinstance(args[1], update.ConstitutiveUpdate):
            raise RuntimeError("Expecting a 'fsm ConstitutiveUpdate' as argument 1, got ", type(args[1]), ".")
        self.ConstitutiveUpdate = args[1]
        if len(args) == 3:
            if args[2] != 'tangent':
                raise RuntimeError("Expecting string 'tangent' as argument 3, got ", args[2], "of type ", type(args[2]), ".")
            self._cpp_object = quadrature_function.QuadratureFunction(self.Mesh, self.Element,
                                            self.ConstitutiveUpdate.cpp_object(), self.ConstitutiveUpdate.w_tangent())
        elif len(args) == 2:
            self._cpp_object = quadrature_function.QuadratureFunction(self.Mesh, self.Element, self.ConstitutiveUpdate.w_stress())
        else:
            raise RuntimeError("Expecting 2 or 3 arguments, got ", len(args), ".")

        ufl.Coefficient.__init__(self, args[0].ufl_function_space(), count=self._cpp_object.id())

        # Set name as given or automatic
        name = kwargs.get("name") or "f_%d" % self.count()
        self._cpp_object.rename(name, "a QuadratureFunction")

    def name(self):
        return self._cpp_object.name()
    
    def cpp_object(self):
        return self._cpp_object


