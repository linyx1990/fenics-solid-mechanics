# -*- coding: utf-8 -*-
"""Main module for FEniCS Solid Mechanics"""

# flake8: noqa

# Copyright (C) 2017 Chris N. Richardson and Garth N. Wells
#
# Distributed under the terms of the GNU Lesser Public License (LGPL),
# either version 3 of the License, or (at your option) any later
# version.

import sys

# Store dl open flags to restore them after import
stored_dlopen_flags = sys.getdlopenflags()

# Developer note: below is related to OpenMPI
# Fix dlopen flags (may need reorganising)
if "linux" in sys.platform:
    # FIXME: What with other platforms?
    try:
        from ctypes import RTLD_NOW, RTLD_GLOBAL
    except ImportError:
        RTLD_NOW = 2
        RTLD_GLOBAL = 256
    sys.setdlopenflags(RTLD_NOW | RTLD_GLOBAL)
del sys

# Reset dl open flags
# sys.setdlopenflags(stored_dlopen_flags)
# del sys

# Import cpp modules
#from .fsm_cpp.quadrature_function import (QuadratureFunction)
#from .fsm_cpp.plasticity_problem import (PlasticityProblem)
from .fsm_cpp.plasticity_model import (PlasticityModel, DruckerPrager, VonMises)
from .fsm_cpp.history_data import (HistoryData)
#from .fsm_cpp.status_update import (StateUpdate, ConstitutiveUpdate)
from .fsm_cpp.status_update import (StateUpdate)
from .fsm_cpp.return_mapping import (ReturnMapping)

from .quadrature import QuadratureFunction
from .update import ConstitutiveUpdate
from .problem import PlasticityProblem

